#include <iostream>
#include <any>
#include <cstdio>
#include <fstream>
#include "atn/SetTransition.cpp"
#include "antlr4-runtime.h"
#include "/Users/ANDREA/Desktop/archivos/andrea/build/CalculadoraLexer.h"
#include "/Users/ANDREA/Desktop/archivos/andrea/build/CalculadoraParser.h"
#include "/Users/ANDREA/Desktop/archivos/andrea/build/CalculadoraBaseVisitor.h"
#include "/Users/ANDREA/Desktop/archivos/andrea/build/CalculadoraEvalVisitor.h"

using namespace antlr4;

int main(int argc, char** argv) {
    std::cout << "Calculadora con Antlr4 en C++" << std::endl;

    //Lectura de txt:
    /*if (argc <= 1)
        return -1;
    std::ifstream is;
    is.open(argv[1]);
    antlr4::ANTLRInputStream input(is);
    CalculadoraLexer lexer(&input);
    antlr4::CommonTokenStream tokens(&lexer);
    CalculadoraParser parser(&tokens);
    auto tree = parser.prog();
    CalculadoraEvalVisitor eval;
    eval.visitProg(tree);*/

    //Lectura por ingreso de l�nea
    std::string line;
    std::cout << "Si desea salir del programa presione Enter o CTRL+D(Linux)\n";
    while (true) {
        std::cout << "> ";
        std::getline(std::cin, line);

        if (line.empty() || std::cin.eof())
            break;

        antlr4::ANTLRInputStream input(line + '\n'); 
        CalculadoraLexer lexer(&input);
        antlr4::CommonTokenStream tokens(&lexer);
        CalculadoraParser parser(&tokens);
        auto tree = parser.prog();
        CalculadoraEvalVisitor eval;
        eval.visitProg(tree);
    }

    return 0;
}