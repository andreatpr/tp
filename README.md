# tp
Integrantes:
 - Andrea Katherina Tapia Pescoran
 - Emily Mendoza Manrique
 - Ayrton Jafet Samaniego Millan

Desarrollamos una calculadora con antlr4 y C++. Usamos el entorno de Visual Studio y construimos el "build" con un archivo CMakeLists.txt mediante cmake. Las implementaciones se hicieron por archivo input .txt y como est� actualmente con lectura de l�neas.

El .exe se encuentra en /build/Debug/MyExample.exe
Funcionamiento con .txt:
![alt text](image.png)

Actualmente, con entrada manual:
![alt text](image-1.png)