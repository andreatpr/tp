grammar Calculadora;

prog: stat+ ;

stat
	: expr NEWLINE		# printExpr
	| ID '=' expr NEWLINE	# assign
	| NEWLINE		# blank
	;

expr
	: expr expr '^'		# Pow
	| expr expr '*'		# Mul
	| expr expr '/'		# Div
	| expr expr '+'		# Add
	| expr expr '-'		# Sub
	| expr 'COS'          # Cos
    | expr 'SIN'          # Sin
    | expr 'TAN'          # Tan
    | expr 'COT'          # Cot
	| expr 'n'		# Neg
	| NUM			# num
	| ID			# id
	;

ID	: [a-zA-Z]+;
NUM	: [0-9]+('.'[0-9]+)?;
NEWLINE : '\r'? '\n';
WS	: [ \t]+ ->skip;
