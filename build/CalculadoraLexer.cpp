
// Generated from Calculadora.g4 by ANTLR 4.13.1


#include "CalculadoraLexer.h"


using namespace antlr4;



using namespace antlr4;

namespace {

struct CalculadoraLexerStaticData final {
  CalculadoraLexerStaticData(std::vector<std::string> ruleNames,
                          std::vector<std::string> channelNames,
                          std::vector<std::string> modeNames,
                          std::vector<std::string> literalNames,
                          std::vector<std::string> symbolicNames)
      : ruleNames(std::move(ruleNames)), channelNames(std::move(channelNames)),
        modeNames(std::move(modeNames)), literalNames(std::move(literalNames)),
        symbolicNames(std::move(symbolicNames)),
        vocabulary(this->literalNames, this->symbolicNames) {}

  CalculadoraLexerStaticData(const CalculadoraLexerStaticData&) = delete;
  CalculadoraLexerStaticData(CalculadoraLexerStaticData&&) = delete;
  CalculadoraLexerStaticData& operator=(const CalculadoraLexerStaticData&) = delete;
  CalculadoraLexerStaticData& operator=(CalculadoraLexerStaticData&&) = delete;

  std::vector<antlr4::dfa::DFA> decisionToDFA;
  antlr4::atn::PredictionContextCache sharedContextCache;
  const std::vector<std::string> ruleNames;
  const std::vector<std::string> channelNames;
  const std::vector<std::string> modeNames;
  const std::vector<std::string> literalNames;
  const std::vector<std::string> symbolicNames;
  const antlr4::dfa::Vocabulary vocabulary;
  antlr4::atn::SerializedATNView serializedATN;
  std::unique_ptr<antlr4::atn::ATN> atn;
};

::antlr4::internal::OnceFlag calculadoralexerLexerOnceFlag;
#if ANTLR4_USE_THREAD_LOCAL_CACHE
static thread_local
#endif
CalculadoraLexerStaticData *calculadoralexerLexerStaticData = nullptr;

void calculadoralexerLexerInitialize() {
#if ANTLR4_USE_THREAD_LOCAL_CACHE
  if (calculadoralexerLexerStaticData != nullptr) {
    return;
  }
#else
  assert(calculadoralexerLexerStaticData == nullptr);
#endif
  auto staticData = std::make_unique<CalculadoraLexerStaticData>(
    std::vector<std::string>{
      "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
      "T__9", "T__10", "ID", "NUM", "NEWLINE", "WS"
    },
    std::vector<std::string>{
      "DEFAULT_TOKEN_CHANNEL", "HIDDEN"
    },
    std::vector<std::string>{
      "DEFAULT_MODE"
    },
    std::vector<std::string>{
      "", "'='", "'^'", "'*'", "'/'", "'+'", "'-'", "'COS'", "'SIN'", "'TAN'", 
      "'COT'", "'n'"
    },
    std::vector<std::string>{
      "", "", "", "", "", "", "", "", "", "", "", "", "ID", "NUM", "NEWLINE", 
      "WS"
    }
  );
  static const int32_t serializedATNSegment[] = {
  	4,0,15,91,6,-1,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
  	6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,2,12,7,12,2,13,7,13,2,14,
  	7,14,1,0,1,0,1,1,1,1,1,2,1,2,1,3,1,3,1,4,1,4,1,5,1,5,1,6,1,6,1,6,1,6,
  	1,7,1,7,1,7,1,7,1,8,1,8,1,8,1,8,1,9,1,9,1,9,1,9,1,10,1,10,1,11,4,11,63,
  	8,11,11,11,12,11,64,1,12,4,12,68,8,12,11,12,12,12,69,1,12,1,12,4,12,74,
  	8,12,11,12,12,12,75,3,12,78,8,12,1,13,3,13,81,8,13,1,13,1,13,1,14,4,14,
  	86,8,14,11,14,12,14,87,1,14,1,14,0,0,15,1,1,3,2,5,3,7,4,9,5,11,6,13,7,
  	15,8,17,9,19,10,21,11,23,12,25,13,27,14,29,15,1,0,3,2,0,65,90,97,122,
  	1,0,48,57,2,0,9,9,32,32,96,0,1,1,0,0,0,0,3,1,0,0,0,0,5,1,0,0,0,0,7,1,
  	0,0,0,0,9,1,0,0,0,0,11,1,0,0,0,0,13,1,0,0,0,0,15,1,0,0,0,0,17,1,0,0,0,
  	0,19,1,0,0,0,0,21,1,0,0,0,0,23,1,0,0,0,0,25,1,0,0,0,0,27,1,0,0,0,0,29,
  	1,0,0,0,1,31,1,0,0,0,3,33,1,0,0,0,5,35,1,0,0,0,7,37,1,0,0,0,9,39,1,0,
  	0,0,11,41,1,0,0,0,13,43,1,0,0,0,15,47,1,0,0,0,17,51,1,0,0,0,19,55,1,0,
  	0,0,21,59,1,0,0,0,23,62,1,0,0,0,25,67,1,0,0,0,27,80,1,0,0,0,29,85,1,0,
  	0,0,31,32,5,61,0,0,32,2,1,0,0,0,33,34,5,94,0,0,34,4,1,0,0,0,35,36,5,42,
  	0,0,36,6,1,0,0,0,37,38,5,47,0,0,38,8,1,0,0,0,39,40,5,43,0,0,40,10,1,0,
  	0,0,41,42,5,45,0,0,42,12,1,0,0,0,43,44,5,67,0,0,44,45,5,79,0,0,45,46,
  	5,83,0,0,46,14,1,0,0,0,47,48,5,83,0,0,48,49,5,73,0,0,49,50,5,78,0,0,50,
  	16,1,0,0,0,51,52,5,84,0,0,52,53,5,65,0,0,53,54,5,78,0,0,54,18,1,0,0,0,
  	55,56,5,67,0,0,56,57,5,79,0,0,57,58,5,84,0,0,58,20,1,0,0,0,59,60,5,110,
  	0,0,60,22,1,0,0,0,61,63,7,0,0,0,62,61,1,0,0,0,63,64,1,0,0,0,64,62,1,0,
  	0,0,64,65,1,0,0,0,65,24,1,0,0,0,66,68,7,1,0,0,67,66,1,0,0,0,68,69,1,0,
  	0,0,69,67,1,0,0,0,69,70,1,0,0,0,70,77,1,0,0,0,71,73,5,46,0,0,72,74,7,
  	1,0,0,73,72,1,0,0,0,74,75,1,0,0,0,75,73,1,0,0,0,75,76,1,0,0,0,76,78,1,
  	0,0,0,77,71,1,0,0,0,77,78,1,0,0,0,78,26,1,0,0,0,79,81,5,13,0,0,80,79,
  	1,0,0,0,80,81,1,0,0,0,81,82,1,0,0,0,82,83,5,10,0,0,83,28,1,0,0,0,84,86,
  	7,2,0,0,85,84,1,0,0,0,86,87,1,0,0,0,87,85,1,0,0,0,87,88,1,0,0,0,88,89,
  	1,0,0,0,89,90,6,14,0,0,90,30,1,0,0,0,7,0,64,69,75,77,80,87,1,6,0,0
  };
  staticData->serializedATN = antlr4::atn::SerializedATNView(serializedATNSegment, sizeof(serializedATNSegment) / sizeof(serializedATNSegment[0]));

  antlr4::atn::ATNDeserializer deserializer;
  staticData->atn = deserializer.deserialize(staticData->serializedATN);

  const size_t count = staticData->atn->getNumberOfDecisions();
  staticData->decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    staticData->decisionToDFA.emplace_back(staticData->atn->getDecisionState(i), i);
  }
  calculadoralexerLexerStaticData = staticData.release();
}

}

CalculadoraLexer::CalculadoraLexer(CharStream *input) : Lexer(input) {
  CalculadoraLexer::initialize();
  _interpreter = new atn::LexerATNSimulator(this, *calculadoralexerLexerStaticData->atn, calculadoralexerLexerStaticData->decisionToDFA, calculadoralexerLexerStaticData->sharedContextCache);
}

CalculadoraLexer::~CalculadoraLexer() {
  delete _interpreter;
}

std::string CalculadoraLexer::getGrammarFileName() const {
  return "Calculadora.g4";
}

const std::vector<std::string>& CalculadoraLexer::getRuleNames() const {
  return calculadoralexerLexerStaticData->ruleNames;
}

const std::vector<std::string>& CalculadoraLexer::getChannelNames() const {
  return calculadoralexerLexerStaticData->channelNames;
}

const std::vector<std::string>& CalculadoraLexer::getModeNames() const {
  return calculadoralexerLexerStaticData->modeNames;
}

const dfa::Vocabulary& CalculadoraLexer::getVocabulary() const {
  return calculadoralexerLexerStaticData->vocabulary;
}

antlr4::atn::SerializedATNView CalculadoraLexer::getSerializedATN() const {
  return calculadoralexerLexerStaticData->serializedATN;
}

const atn::ATN& CalculadoraLexer::getATN() const {
  return *calculadoralexerLexerStaticData->atn;
}




void CalculadoraLexer::initialize() {
#if ANTLR4_USE_THREAD_LOCAL_CACHE
  calculadoralexerLexerInitialize();
#else
  ::antlr4::internal::call_once(calculadoralexerLexerOnceFlag, calculadoralexerLexerInitialize);
#endif
}
