#include "/Users/ANDREA/Desktop/archivos/andrea/build/CalculadoraBaseVisitor.h"
#include <iostream> 
#include <map>
#include <any>
#include <variant>
#include <string>
#include <cmath>
#include <algorithm>
#include "/Users/ANDREA/Desktop/archivos/andrea/build/CalculadoraParser.h"

class CalculadoraEvalVisitor : public CalculadoraBaseVisitor {
	std::any value = 42;
    std::map<std::string, std::any> memory;
public:
    std::any visitAssign(CalculadoraParser::AssignContext *ctx) override {
        std::string id = ctx->ID()->getText();
        auto value = visit(ctx->expr());
        memory[id] = value;
        
        return value;
    }
    std::any visitPrintExpr(CalculadoraParser::PrintExprContext* ctx) override{
		auto value = std::any_cast<double>(visit(ctx->expr()));
        std::cout << value << "\n";
		return std::any();
	}

    std::any visitNum(CalculadoraParser::NumContext* ctx) override{
		return std::any(std::stod(ctx->NUM()->getText()));
	}

	std::any visitId(CalculadoraParser::IdContext* ctx) override{
		std::string id = ctx->ID()->getText();
		if (memory.count(id)) return memory[id];
		return std::any();
	}

	std::any visitPow(CalculadoraParser::PowContext* ctx) override {
		double left = std::any_cast<double>(visit(ctx->expr(0)));
		double right = std::any_cast<double>(visit(ctx->expr(1)));
		double res = pow(left, right);
		return std::any(res);
	}

	std::any visitMul(CalculadoraParser::MulContext* ctx) override {
		double left = std::any_cast<double>(visit(ctx->expr(0)));
		double right = std::any_cast<double>(visit(ctx->expr(1)));
		return std::any(left * right);
	}

	std::any visitDiv(CalculadoraParser::DivContext* ctx) override {
		double left = std::any_cast<double>(visit(ctx->expr(0)));
		double right = std::any_cast<double>(visit(ctx->expr(1)));

		return std::any(left/right);
	}

	std::any visitAdd(CalculadoraParser::AddContext* ctx) override {
		double left = std::any_cast<double>(visit(ctx->expr(0)));
		double right = std::any_cast<double>(visit(ctx->expr(1)));

		return std::any(left + right);
	}

	std::any visitSub(CalculadoraParser::SubContext* ctx) override {
		double left = std::any_cast<double>(visit(ctx->expr(0)));
		double right = std::any_cast<double>(visit(ctx->expr(1)));

		return std::any(left - right);
	}

	std::any visitProg(CalculadoraParser::ProgContext* ctx) {
		return visitChildren(ctx);
	}

	std::any visitNeg(CalculadoraParser::NegContext* ctx) override {
		double left = std::any_cast<double>(visit(ctx->expr()));
		return std::any(left * -1);
	}

	std::any visitCos(CalculadoraParser::CosContext* ctx) override {
		double arg = std::any_cast<double>(visit(ctx->expr()));
		return std::any(cos(arg));
	}

	std::any visitSin(CalculadoraParser::SinContext* ctx) override {
		double arg = std::any_cast<double>(visit(ctx->expr()));
		return std::any(sin(arg));
	}
	std::any visitTan(CalculadoraParser::TanContext* ctx) override {
		double arg = std::any_cast<double>(visit(ctx->expr()));
		return std::any(tan(arg));
	}
	std::any visitCot(CalculadoraParser::CotContext* ctx) override {
		double arg = std::any_cast<double>(visit(ctx->expr()));
		return std::any(1.0 / tan(arg));
	}

};