
// Generated from Calculadora.g4 by ANTLR 4.13.1

#pragma once


#include "antlr4-runtime.h"
#include "CalculadoraParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by CalculadoraParser.
 */
class  CalculadoraVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by CalculadoraParser.
   */
    virtual std::any visitProg(CalculadoraParser::ProgContext *context) = 0;

    virtual std::any visitPrintExpr(CalculadoraParser::PrintExprContext *context) = 0;

    virtual std::any visitAssign(CalculadoraParser::AssignContext *context) = 0;

    virtual std::any visitBlank(CalculadoraParser::BlankContext *context) = 0;

    virtual std::any visitDiv(CalculadoraParser::DivContext *context) = 0;

    virtual std::any visitAdd(CalculadoraParser::AddContext *context) = 0;

    virtual std::any visitTan(CalculadoraParser::TanContext *context) = 0;

    virtual std::any visitSub(CalculadoraParser::SubContext *context) = 0;

    virtual std::any visitNeg(CalculadoraParser::NegContext *context) = 0;

    virtual std::any visitMul(CalculadoraParser::MulContext *context) = 0;

    virtual std::any visitCos(CalculadoraParser::CosContext *context) = 0;

    virtual std::any visitNum(CalculadoraParser::NumContext *context) = 0;

    virtual std::any visitPow(CalculadoraParser::PowContext *context) = 0;

    virtual std::any visitSin(CalculadoraParser::SinContext *context) = 0;

    virtual std::any visitCot(CalculadoraParser::CotContext *context) = 0;

    virtual std::any visitId(CalculadoraParser::IdContext *context) = 0;


};

