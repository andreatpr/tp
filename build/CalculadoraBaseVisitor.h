
// Generated from Calculadora.g4 by ANTLR 4.13.1

#pragma once


#include "antlr4-runtime.h"
#include "CalculadoraVisitor.h"


/**
 * This class provides an empty implementation of CalculadoraVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  CalculadoraBaseVisitor : public CalculadoraVisitor {
public:

  virtual std::any visitProg(CalculadoraParser::ProgContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitPrintExpr(CalculadoraParser::PrintExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitAssign(CalculadoraParser::AssignContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitBlank(CalculadoraParser::BlankContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitDiv(CalculadoraParser::DivContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitAdd(CalculadoraParser::AddContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitTan(CalculadoraParser::TanContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitSub(CalculadoraParser::SubContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitNeg(CalculadoraParser::NegContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitMul(CalculadoraParser::MulContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitCos(CalculadoraParser::CosContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitNum(CalculadoraParser::NumContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitPow(CalculadoraParser::PowContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitSin(CalculadoraParser::SinContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitCot(CalculadoraParser::CotContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitId(CalculadoraParser::IdContext *ctx) override {
    return visitChildren(ctx);
  }


};

