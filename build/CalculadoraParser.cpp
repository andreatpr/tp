
// Generated from Calculadora.g4 by ANTLR 4.13.1


#include "CalculadoraVisitor.h"

#include "CalculadoraParser.h"


using namespace antlrcpp;

using namespace antlr4;

namespace {

struct CalculadoraParserStaticData final {
  CalculadoraParserStaticData(std::vector<std::string> ruleNames,
                        std::vector<std::string> literalNames,
                        std::vector<std::string> symbolicNames)
      : ruleNames(std::move(ruleNames)), literalNames(std::move(literalNames)),
        symbolicNames(std::move(symbolicNames)),
        vocabulary(this->literalNames, this->symbolicNames) {}

  CalculadoraParserStaticData(const CalculadoraParserStaticData&) = delete;
  CalculadoraParserStaticData(CalculadoraParserStaticData&&) = delete;
  CalculadoraParserStaticData& operator=(const CalculadoraParserStaticData&) = delete;
  CalculadoraParserStaticData& operator=(CalculadoraParserStaticData&&) = delete;

  std::vector<antlr4::dfa::DFA> decisionToDFA;
  antlr4::atn::PredictionContextCache sharedContextCache;
  const std::vector<std::string> ruleNames;
  const std::vector<std::string> literalNames;
  const std::vector<std::string> symbolicNames;
  const antlr4::dfa::Vocabulary vocabulary;
  antlr4::atn::SerializedATNView serializedATN;
  std::unique_ptr<antlr4::atn::ATN> atn;
};

::antlr4::internal::OnceFlag calculadoraParserOnceFlag;
#if ANTLR4_USE_THREAD_LOCAL_CACHE
static thread_local
#endif
CalculadoraParserStaticData *calculadoraParserStaticData = nullptr;

void calculadoraParserInitialize() {
#if ANTLR4_USE_THREAD_LOCAL_CACHE
  if (calculadoraParserStaticData != nullptr) {
    return;
  }
#else
  assert(calculadoraParserStaticData == nullptr);
#endif
  auto staticData = std::make_unique<CalculadoraParserStaticData>(
    std::vector<std::string>{
      "prog", "stat", "expr"
    },
    std::vector<std::string>{
      "", "'='", "'^'", "'*'", "'/'", "'+'", "'-'", "'COS'", "'SIN'", "'TAN'", 
      "'COT'", "'n'"
    },
    std::vector<std::string>{
      "", "", "", "", "", "", "", "", "", "", "", "", "ID", "NUM", "NEWLINE", 
      "WS"
    }
  );
  static const int32_t serializedATNSegment[] = {
  	4,1,15,63,2,0,7,0,2,1,7,1,2,2,7,2,1,0,4,0,8,8,0,11,0,12,0,9,1,1,1,1,1,
  	1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,21,8,1,1,2,1,2,1,2,3,2,26,8,2,1,2,1,2,1,
  	2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,
  	1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,5,2,58,8,2,10,2,12,2,61,9,2,1,
  	2,0,1,4,3,0,2,4,0,0,73,0,7,1,0,0,0,2,20,1,0,0,0,4,25,1,0,0,0,6,8,3,2,
  	1,0,7,6,1,0,0,0,8,9,1,0,0,0,9,7,1,0,0,0,9,10,1,0,0,0,10,1,1,0,0,0,11,
  	12,3,4,2,0,12,13,5,14,0,0,13,21,1,0,0,0,14,15,5,12,0,0,15,16,5,1,0,0,
  	16,17,3,4,2,0,17,18,5,14,0,0,18,21,1,0,0,0,19,21,5,14,0,0,20,11,1,0,0,
  	0,20,14,1,0,0,0,20,19,1,0,0,0,21,3,1,0,0,0,22,23,6,2,-1,0,23,26,5,13,
  	0,0,24,26,5,12,0,0,25,22,1,0,0,0,25,24,1,0,0,0,26,59,1,0,0,0,27,28,10,
  	12,0,0,28,29,3,4,2,0,29,30,5,2,0,0,30,58,1,0,0,0,31,32,10,11,0,0,32,33,
  	3,4,2,0,33,34,5,3,0,0,34,58,1,0,0,0,35,36,10,10,0,0,36,37,3,4,2,0,37,
  	38,5,4,0,0,38,58,1,0,0,0,39,40,10,9,0,0,40,41,3,4,2,0,41,42,5,5,0,0,42,
  	58,1,0,0,0,43,44,10,8,0,0,44,45,3,4,2,0,45,46,5,6,0,0,46,58,1,0,0,0,47,
  	48,10,7,0,0,48,58,5,7,0,0,49,50,10,6,0,0,50,58,5,8,0,0,51,52,10,5,0,0,
  	52,58,5,9,0,0,53,54,10,4,0,0,54,58,5,10,0,0,55,56,10,3,0,0,56,58,5,11,
  	0,0,57,27,1,0,0,0,57,31,1,0,0,0,57,35,1,0,0,0,57,39,1,0,0,0,57,43,1,0,
  	0,0,57,47,1,0,0,0,57,49,1,0,0,0,57,51,1,0,0,0,57,53,1,0,0,0,57,55,1,0,
  	0,0,58,61,1,0,0,0,59,57,1,0,0,0,59,60,1,0,0,0,60,5,1,0,0,0,61,59,1,0,
  	0,0,5,9,20,25,57,59
  };
  staticData->serializedATN = antlr4::atn::SerializedATNView(serializedATNSegment, sizeof(serializedATNSegment) / sizeof(serializedATNSegment[0]));

  antlr4::atn::ATNDeserializer deserializer;
  staticData->atn = deserializer.deserialize(staticData->serializedATN);

  const size_t count = staticData->atn->getNumberOfDecisions();
  staticData->decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    staticData->decisionToDFA.emplace_back(staticData->atn->getDecisionState(i), i);
  }
  calculadoraParserStaticData = staticData.release();
}

}

CalculadoraParser::CalculadoraParser(TokenStream *input) : CalculadoraParser(input, antlr4::atn::ParserATNSimulatorOptions()) {}

CalculadoraParser::CalculadoraParser(TokenStream *input, const antlr4::atn::ParserATNSimulatorOptions &options) : Parser(input) {
  CalculadoraParser::initialize();
  _interpreter = new atn::ParserATNSimulator(this, *calculadoraParserStaticData->atn, calculadoraParserStaticData->decisionToDFA, calculadoraParserStaticData->sharedContextCache, options);
}

CalculadoraParser::~CalculadoraParser() {
  delete _interpreter;
}

const atn::ATN& CalculadoraParser::getATN() const {
  return *calculadoraParserStaticData->atn;
}

std::string CalculadoraParser::getGrammarFileName() const {
  return "Calculadora.g4";
}

const std::vector<std::string>& CalculadoraParser::getRuleNames() const {
  return calculadoraParserStaticData->ruleNames;
}

const dfa::Vocabulary& CalculadoraParser::getVocabulary() const {
  return calculadoraParserStaticData->vocabulary;
}

antlr4::atn::SerializedATNView CalculadoraParser::getSerializedATN() const {
  return calculadoraParserStaticData->serializedATN;
}


//----------------- ProgContext ------------------------------------------------------------------

CalculadoraParser::ProgContext::ProgContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CalculadoraParser::StatContext *> CalculadoraParser::ProgContext::stat() {
  return getRuleContexts<CalculadoraParser::StatContext>();
}

CalculadoraParser::StatContext* CalculadoraParser::ProgContext::stat(size_t i) {
  return getRuleContext<CalculadoraParser::StatContext>(i);
}


size_t CalculadoraParser::ProgContext::getRuleIndex() const {
  return CalculadoraParser::RuleProg;
}


std::any CalculadoraParser::ProgContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitProg(this);
  else
    return visitor->visitChildren(this);
}

CalculadoraParser::ProgContext* CalculadoraParser::prog() {
  ProgContext *_localctx = _tracker.createInstance<ProgContext>(_ctx, getState());
  enterRule(_localctx, 0, CalculadoraParser::RuleProg);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(7); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(6);
      stat();
      setState(9); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & 28672) != 0));
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatContext ------------------------------------------------------------------

CalculadoraParser::StatContext::StatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t CalculadoraParser::StatContext::getRuleIndex() const {
  return CalculadoraParser::RuleStat;
}

void CalculadoraParser::StatContext::copyFrom(StatContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- BlankContext ------------------------------------------------------------------

tree::TerminalNode* CalculadoraParser::BlankContext::NEWLINE() {
  return getToken(CalculadoraParser::NEWLINE, 0);
}

CalculadoraParser::BlankContext::BlankContext(StatContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::BlankContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitBlank(this);
  else
    return visitor->visitChildren(this);
}
//----------------- PrintExprContext ------------------------------------------------------------------

CalculadoraParser::ExprContext* CalculadoraParser::PrintExprContext::expr() {
  return getRuleContext<CalculadoraParser::ExprContext>(0);
}

tree::TerminalNode* CalculadoraParser::PrintExprContext::NEWLINE() {
  return getToken(CalculadoraParser::NEWLINE, 0);
}

CalculadoraParser::PrintExprContext::PrintExprContext(StatContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::PrintExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitPrintExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- AssignContext ------------------------------------------------------------------

tree::TerminalNode* CalculadoraParser::AssignContext::ID() {
  return getToken(CalculadoraParser::ID, 0);
}

CalculadoraParser::ExprContext* CalculadoraParser::AssignContext::expr() {
  return getRuleContext<CalculadoraParser::ExprContext>(0);
}

tree::TerminalNode* CalculadoraParser::AssignContext::NEWLINE() {
  return getToken(CalculadoraParser::NEWLINE, 0);
}

CalculadoraParser::AssignContext::AssignContext(StatContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::AssignContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitAssign(this);
  else
    return visitor->visitChildren(this);
}
CalculadoraParser::StatContext* CalculadoraParser::stat() {
  StatContext *_localctx = _tracker.createInstance<StatContext>(_ctx, getState());
  enterRule(_localctx, 2, CalculadoraParser::RuleStat);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(20);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 1, _ctx)) {
    case 1: {
      _localctx = _tracker.createInstance<CalculadoraParser::PrintExprContext>(_localctx);
      enterOuterAlt(_localctx, 1);
      setState(11);
      expr(0);
      setState(12);
      match(CalculadoraParser::NEWLINE);
      break;
    }

    case 2: {
      _localctx = _tracker.createInstance<CalculadoraParser::AssignContext>(_localctx);
      enterOuterAlt(_localctx, 2);
      setState(14);
      match(CalculadoraParser::ID);
      setState(15);
      match(CalculadoraParser::T__0);
      setState(16);
      expr(0);
      setState(17);
      match(CalculadoraParser::NEWLINE);
      break;
    }

    case 3: {
      _localctx = _tracker.createInstance<CalculadoraParser::BlankContext>(_localctx);
      enterOuterAlt(_localctx, 3);
      setState(19);
      match(CalculadoraParser::NEWLINE);
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExprContext ------------------------------------------------------------------

CalculadoraParser::ExprContext::ExprContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t CalculadoraParser::ExprContext::getRuleIndex() const {
  return CalculadoraParser::RuleExpr;
}

void CalculadoraParser::ExprContext::copyFrom(ExprContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- DivContext ------------------------------------------------------------------

std::vector<CalculadoraParser::ExprContext *> CalculadoraParser::DivContext::expr() {
  return getRuleContexts<CalculadoraParser::ExprContext>();
}

CalculadoraParser::ExprContext* CalculadoraParser::DivContext::expr(size_t i) {
  return getRuleContext<CalculadoraParser::ExprContext>(i);
}

CalculadoraParser::DivContext::DivContext(ExprContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::DivContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitDiv(this);
  else
    return visitor->visitChildren(this);
}
//----------------- AddContext ------------------------------------------------------------------

std::vector<CalculadoraParser::ExprContext *> CalculadoraParser::AddContext::expr() {
  return getRuleContexts<CalculadoraParser::ExprContext>();
}

CalculadoraParser::ExprContext* CalculadoraParser::AddContext::expr(size_t i) {
  return getRuleContext<CalculadoraParser::ExprContext>(i);
}

CalculadoraParser::AddContext::AddContext(ExprContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::AddContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitAdd(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TanContext ------------------------------------------------------------------

CalculadoraParser::ExprContext* CalculadoraParser::TanContext::expr() {
  return getRuleContext<CalculadoraParser::ExprContext>(0);
}

CalculadoraParser::TanContext::TanContext(ExprContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::TanContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitTan(this);
  else
    return visitor->visitChildren(this);
}
//----------------- SubContext ------------------------------------------------------------------

std::vector<CalculadoraParser::ExprContext *> CalculadoraParser::SubContext::expr() {
  return getRuleContexts<CalculadoraParser::ExprContext>();
}

CalculadoraParser::ExprContext* CalculadoraParser::SubContext::expr(size_t i) {
  return getRuleContext<CalculadoraParser::ExprContext>(i);
}

CalculadoraParser::SubContext::SubContext(ExprContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::SubContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitSub(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NegContext ------------------------------------------------------------------

CalculadoraParser::ExprContext* CalculadoraParser::NegContext::expr() {
  return getRuleContext<CalculadoraParser::ExprContext>(0);
}

CalculadoraParser::NegContext::NegContext(ExprContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::NegContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitNeg(this);
  else
    return visitor->visitChildren(this);
}
//----------------- MulContext ------------------------------------------------------------------

std::vector<CalculadoraParser::ExprContext *> CalculadoraParser::MulContext::expr() {
  return getRuleContexts<CalculadoraParser::ExprContext>();
}

CalculadoraParser::ExprContext* CalculadoraParser::MulContext::expr(size_t i) {
  return getRuleContext<CalculadoraParser::ExprContext>(i);
}

CalculadoraParser::MulContext::MulContext(ExprContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::MulContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitMul(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CosContext ------------------------------------------------------------------

CalculadoraParser::ExprContext* CalculadoraParser::CosContext::expr() {
  return getRuleContext<CalculadoraParser::ExprContext>(0);
}

CalculadoraParser::CosContext::CosContext(ExprContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::CosContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitCos(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NumContext ------------------------------------------------------------------

tree::TerminalNode* CalculadoraParser::NumContext::NUM() {
  return getToken(CalculadoraParser::NUM, 0);
}

CalculadoraParser::NumContext::NumContext(ExprContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::NumContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitNum(this);
  else
    return visitor->visitChildren(this);
}
//----------------- PowContext ------------------------------------------------------------------

std::vector<CalculadoraParser::ExprContext *> CalculadoraParser::PowContext::expr() {
  return getRuleContexts<CalculadoraParser::ExprContext>();
}

CalculadoraParser::ExprContext* CalculadoraParser::PowContext::expr(size_t i) {
  return getRuleContext<CalculadoraParser::ExprContext>(i);
}

CalculadoraParser::PowContext::PowContext(ExprContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::PowContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitPow(this);
  else
    return visitor->visitChildren(this);
}
//----------------- SinContext ------------------------------------------------------------------

CalculadoraParser::ExprContext* CalculadoraParser::SinContext::expr() {
  return getRuleContext<CalculadoraParser::ExprContext>(0);
}

CalculadoraParser::SinContext::SinContext(ExprContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::SinContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitSin(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CotContext ------------------------------------------------------------------

CalculadoraParser::ExprContext* CalculadoraParser::CotContext::expr() {
  return getRuleContext<CalculadoraParser::ExprContext>(0);
}

CalculadoraParser::CotContext::CotContext(ExprContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::CotContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitCot(this);
  else
    return visitor->visitChildren(this);
}
//----------------- IdContext ------------------------------------------------------------------

tree::TerminalNode* CalculadoraParser::IdContext::ID() {
  return getToken(CalculadoraParser::ID, 0);
}

CalculadoraParser::IdContext::IdContext(ExprContext *ctx) { copyFrom(ctx); }


std::any CalculadoraParser::IdContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CalculadoraVisitor*>(visitor))
    return parserVisitor->visitId(this);
  else
    return visitor->visitChildren(this);
}

CalculadoraParser::ExprContext* CalculadoraParser::expr() {
   return expr(0);
}

CalculadoraParser::ExprContext* CalculadoraParser::expr(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  CalculadoraParser::ExprContext *_localctx = _tracker.createInstance<ExprContext>(_ctx, parentState);
  CalculadoraParser::ExprContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 4;
  enterRecursionRule(_localctx, 4, CalculadoraParser::RuleExpr, precedence);

    

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(25);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case CalculadoraParser::NUM: {
        _localctx = _tracker.createInstance<NumContext>(_localctx);
        _ctx = _localctx;
        previousContext = _localctx;

        setState(23);
        match(CalculadoraParser::NUM);
        break;
      }

      case CalculadoraParser::ID: {
        _localctx = _tracker.createInstance<IdContext>(_localctx);
        _ctx = _localctx;
        previousContext = _localctx;
        setState(24);
        match(CalculadoraParser::ID);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    _ctx->stop = _input->LT(-1);
    setState(59);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(57);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 3, _ctx)) {
        case 1: {
          auto newContext = _tracker.createInstance<PowContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(27);

          if (!(precpred(_ctx, 12))) throw FailedPredicateException(this, "precpred(_ctx, 12)");
          setState(28);
          expr(0);
          setState(29);
          match(CalculadoraParser::T__1);
          break;
        }

        case 2: {
          auto newContext = _tracker.createInstance<MulContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(31);

          if (!(precpred(_ctx, 11))) throw FailedPredicateException(this, "precpred(_ctx, 11)");
          setState(32);
          expr(0);
          setState(33);
          match(CalculadoraParser::T__2);
          break;
        }

        case 3: {
          auto newContext = _tracker.createInstance<DivContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(35);

          if (!(precpred(_ctx, 10))) throw FailedPredicateException(this, "precpred(_ctx, 10)");
          setState(36);
          expr(0);
          setState(37);
          match(CalculadoraParser::T__3);
          break;
        }

        case 4: {
          auto newContext = _tracker.createInstance<AddContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(39);

          if (!(precpred(_ctx, 9))) throw FailedPredicateException(this, "precpred(_ctx, 9)");
          setState(40);
          expr(0);
          setState(41);
          match(CalculadoraParser::T__4);
          break;
        }

        case 5: {
          auto newContext = _tracker.createInstance<SubContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(43);

          if (!(precpred(_ctx, 8))) throw FailedPredicateException(this, "precpred(_ctx, 8)");
          setState(44);
          expr(0);
          setState(45);
          match(CalculadoraParser::T__5);
          break;
        }

        case 6: {
          auto newContext = _tracker.createInstance<CosContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(47);

          if (!(precpred(_ctx, 7))) throw FailedPredicateException(this, "precpred(_ctx, 7)");
          setState(48);
          match(CalculadoraParser::T__6);
          break;
        }

        case 7: {
          auto newContext = _tracker.createInstance<SinContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(49);

          if (!(precpred(_ctx, 6))) throw FailedPredicateException(this, "precpred(_ctx, 6)");
          setState(50);
          match(CalculadoraParser::T__7);
          break;
        }

        case 8: {
          auto newContext = _tracker.createInstance<TanContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(51);

          if (!(precpred(_ctx, 5))) throw FailedPredicateException(this, "precpred(_ctx, 5)");
          setState(52);
          match(CalculadoraParser::T__8);
          break;
        }

        case 9: {
          auto newContext = _tracker.createInstance<CotContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(53);

          if (!(precpred(_ctx, 4))) throw FailedPredicateException(this, "precpred(_ctx, 4)");
          setState(54);
          match(CalculadoraParser::T__9);
          break;
        }

        case 10: {
          auto newContext = _tracker.createInstance<NegContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(55);

          if (!(precpred(_ctx, 3))) throw FailedPredicateException(this, "precpred(_ctx, 3)");
          setState(56);
          match(CalculadoraParser::T__10);
          break;
        }

        default:
          break;
        } 
      }
      setState(61);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

bool CalculadoraParser::sempred(RuleContext *context, size_t ruleIndex, size_t predicateIndex) {
  switch (ruleIndex) {
    case 2: return exprSempred(antlrcpp::downCast<ExprContext *>(context), predicateIndex);

  default:
    break;
  }
  return true;
}

bool CalculadoraParser::exprSempred(ExprContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 0: return precpred(_ctx, 12);
    case 1: return precpred(_ctx, 11);
    case 2: return precpred(_ctx, 10);
    case 3: return precpred(_ctx, 9);
    case 4: return precpred(_ctx, 8);
    case 5: return precpred(_ctx, 7);
    case 6: return precpred(_ctx, 6);
    case 7: return precpred(_ctx, 5);
    case 8: return precpred(_ctx, 4);
    case 9: return precpred(_ctx, 3);

  default:
    break;
  }
  return true;
}

void CalculadoraParser::initialize() {
#if ANTLR4_USE_THREAD_LOCAL_CACHE
  calculadoraParserInitialize();
#else
  ::antlr4::internal::call_once(calculadoraParserOnceFlag, calculadoraParserInitialize);
#endif
}
